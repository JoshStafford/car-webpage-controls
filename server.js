var express = require('express');
var spawn = require('child_process').spawn;
var py = require('python-shell');

var app = express();
var server = app.listen(2222);
console.log('Listening on port 2222');

app.use(express.static('public'));

var socket = require('socket.io');

var io = socket(server);

io.sockets.on('connection', newConnection);

function newConnection(socket){
  console.log('New Connection: ' + socket.id);

  socket.on('left', function(){

    py.run('left.py', function (err) {
      if (err) throw err;
      console.log('Left turn.');
    });

  })

  socket.on('right', function(){

    py.run('right.py', function (err) {
      if (err) throw err;
      console.log('Right turn.');
    });

  })

  socket.on('forward', function(){

    py.run('forward.py', function (err) {
      if (err) throw err;
      console.log('Forward.');
    });

  })

  socket.on('back', function(){

    py.run('back.py', function (err) {
      if (err) throw err;
      console.log('Back.');
    });

  })

}
